#!/bin/bash

# Warning: This will delete any existing campaigns created in the standard way.
# This is useful when testing or otherwise wanting to remove existing campaigns.

# remove the auto-generated application.js
rm static/js/application.js

# remove the campaign cache
rm -r static/campaigns/.cache

# remove the most common campaign #1, ignore others to keep this simple
rm -r static/campaigns/1

# remove any campaign databases, photo files, etc., excluding any .gitkeep files
find static/campaigns/ -type f \( -not -name ".gitkeep" \) -delete
